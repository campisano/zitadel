#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

IMAGE="ghcr.io/zitadel/zitadel:v2.49.1"

echo "${IMAGE}"
